/*
 * ******************************************************** {{{1 ***********
 *   Copyright © 2015 … 2020 "Martin Krischik" «krischik@users.sourceforge.net»
 * *************************************************************************
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/
 * ******************************************************** }}}1 ***********
 *
 */

apply plugin: 'com.android.application'
apply plugin: 'kotlin-android'

android {
   compileSdkVersion Compile_Sdk_Version
   buildToolsVersion Built_Tools_Version

   defaultConfig {
      applicationId "com.krischik.fit_import"
      minSdkVersion Min_Sdk_Version
      targetSdkVersion Target_Sdk_Version
      versionCode Version_Number
      versionName Version_Name
      testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
      testApplicationId 'com.krischik.fit_import.test'
      javaCompileOptions {
         annotationProcessorOptions {
            arguments = [
                "androidManifestFile": "$projectDir/src/main/AndroidManifest.xml".toString()
            ]
         } // annotationProcessorOptions
      } // javaCompileOptions
   } // defaultConfig
   signingConfigs {
      debug {
         storeFile file (System.getenv ("KEY_STORE"))
         storePassword System.getenv ("KEY_STOREPASS")
         keyAlias System.getenv ("KEY_ALIAS")
         keyPassword System.getenv ("KEY_KEYPASS")
      } // debug
      release {
         storeFile file (System.getenv ("KEY_STORE"))
         storePassword System.getenv ("KEY_STOREPASS")
         keyAlias System.getenv ("KEY_ALIAS")
         keyPassword System.getenv ("KEY_KEYPASS")
      } // release
   } // signingConfigs
   sourceSets {
      main {
         java.srcDirs += 'src/main/kotlin'
      } // main
      test {
	 java.srcDirs += 'src/test/kotlin'
	 resources.srcDirs += 'src/test/resources'
      } // test
      androidTest {
         java.srcDirs += 'src/androidTest/kotlin'
      } // androidTest
   } // sourceSets
   buildTypes {
      release {
         proguardFiles getDefaultProguardFile ('proguard-android.txt'), 'proguard-rules.pro'
         debuggable false
         signingConfig signingConfigs.release
         minifyEnabled = true
      } // release
      debug {
         debuggable true
         signingConfig signingConfigs.debug
      } // debug
   } // buildTypes
   compileOptions {
      sourceCompatibility JavaVersion.VERSION_1_8
      targetCompatibility JavaVersion.VERSION_1_8
   } // compileOptions
   productFlavors {
   } // debug
   lintOptions {
      abortOnError false
   } // lintOptions
} // android

dependencies {
   annotationProcessor (
      [
         group: 'org.androidannotations', 
         name: 'androidannotations',
         version: Androidannotations_Version
      ]
   ) // annotationProcessor

   implementation project (':Android-Lib')
   implementation project (':Java-Lib')

   implementation (
      [
         group: 'org.androidannotations', 
         name: 'androidannotations-api', 
         version: Androidannotations_Version
      ],
      [group: 'com.android.support', name: 'appcompat-v7', version: Androidsupport_Version],
      [group: 'com.android.support', name: 'support-v4', version: Androidsupport_Version],
      [group: 'com.google.android.gms', name: 'play-services', version: '12.0.1'],
      [group: 'org.jetbrains', name: 'annotations', version: JetBrains_Annotations_Version]
   ) // implementation

   testImplementation (
      [group: 'junit', name: 'junit', version: JUnit_Version]
   ) // testImplementation

   androidTestImplementation project (':Android-TestLib')

   androidTestImplementation (
      [group: 'com.github.rtyley', name: 'android-screenshot-celebrity', version: '1.9'],
      [group: 'com.jayway.android.robotium', name: 'robotium-solo', version: '5.6.3'],
      [group: 'junit', name: 'junit', version: JUnit_Version]
   ) // androidTestImplementation
} // dependencies

// vim: set nowrap tabstop=8 shiftwidth=3 softtabstop=3 expandtab :
// vim: set textwidth=0 filetype=groovy foldmethod=marker nospell :
