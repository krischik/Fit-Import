/* ********************************************************** {{{1 **********
 * Copyright © 2015 \u2026 2019 "Martin Krischik" «krischik@users.sourceforge.net»
 * ************************************************************************** This program is
 * free software: you can redistribute it and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see http://www.gnu.org/licenses/
 * ********************************************************** }}}1 *********/

package com.krischik.fit_import;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * <p> </p>
 *
 * @author "Martin Krischik" «krischik@users.sourceforge.net»
 * @version 1.0
 * @since 1.0
 */
@SuppressWarnings ("WeakerAccess")
@org.androidannotations.annotations.EActivity (R.layout.main_activity)
public class MainActivity
   extends android.support.v7.app.AppCompatActivity
   implements IMainFragment,
	      IMainActivity
{
   /**
    * <p> TAG as class name for logging </p>
    */
   private static final String TAG = com.krischik.Log.getLogTag (MainActivity.class);
   /**
    * <p>Google FIT Model</p>
    */
   @NotNull
   private GoogleFit googleFit;
   /**
    * <p> Calculator fragment </p>
    */
   // @Nullable
   @org.androidannotations.annotations.FragmentById (R.id.Main_Fragment)
   protected MainFragment mainFragment;

   @org.androidannotations.annotations.AfterViews
   protected void afterViews ()
   {
      com.krischik.Log.d (TAG, "+ afterViews");

      if (mainFragment != null)
      {
	 mainFragment.setGoogleFit (googleFit);
      } // if

      com.krischik.Log.d (TAG, "- afterViews");
      return;
   } // doConnect

   /**
    * <p>we are connected to Google Fit (or not);
    *
    * @param connected
    *    true when we are connected
    */
   @Override
   public void doConnect (final boolean connected)
   {
      com.krischik.Log.d (TAG, "+ doConnect \n> connected =" + connected);

      if (mainFragment != null)
      {
	 mainFragment.doConnect (connected);
      } // if

      com.krischik.Log.d (TAG, "- doConnect");
      return;
   } // doConnect

   /**
    * <p>disconnect from Google Fit.</p>
    */
   @Override
   public void doConnectButton ()
   {
      com.krischik.Log.d (TAG, "+ doConnectButton");

      if (mainFragment != null)
      {
	 mainFragment.doConnectButton ();
      } // if

      com.krischik.Log.d (TAG, "- doConnectButton");
      return;
   } // doConnectButton

   /**
    * <p>the import ketfit button has been clicked.</p>
    */
   @Override
   public void doKetfitButton ()
   {
      com.krischik.Log.d (TAG, "+ doKetfitButton");

      if (mainFragment != null)
      {
	 mainFragment.doKetfitButton ();
      } // if

      com.krischik.Log.d (TAG, "doKetfitButton");
      return;
   } // doConnect

   /**
    * <p>the import withings button has been clicked.</p>
    */
   @Override
   public void doWithingsButton ()
   {
      com.krischik.Log.d (TAG, "+ doWithingsButton");

      if (mainFragment != null)
      {
	 mainFragment.doWithingsButton ();
      } // if

      com.krischik.Log.e (TAG, "- doWithingsButton");
      return;
   } // doWithingsButton

   /**
    * <p>the activity</p>
    *
    * @return actvivity
    */
   @Override
   public android.support.v4.app.FragmentActivity getActivity ()
   {
      return this;
   } // getActivity

   /**
    * <p>Google FIT Model</p>
    */
   @Override @NotNull
   public com.krischik.fit_import.GoogleFit getGoogleFit ()
   {
      return googleFit;
   }

   /**
    * <p>remember if we are connected</p>
    *
    * @return true when we are connected
    */
   @Override public boolean isConnected ()
   {
      return googleFit.isConnected ();
   } // doConnectButton

   @Override
   protected void onActivityResult (
      final int requestCode,
      final int resultCode,
      final android.content.Intent data)
   {
      com.krischik.Log.d (
	 TAG,
	 "+ onActivityResult" +
	    "\n> requestCode = " + requestCode +
	    "\n> resultCode = " + resultCode +
	    "\n> data = " + data +
	    "\n>");
      if (requestCode == GoogleFit.Request_OAuth)
      {
	 googleFit.doConnect (resultCode);
      } // if

      com.krischik.Log.d (TAG, "- onActivityResult");
      return;
   } // onActivityResult

   @Override
   protected void onCreate (@Nullable final android.os.Bundle savedInstanceState)
   {
      com.krischik.Log.d (
	 TAG,
	 "+ onCreate" + "\n> savedInstanceState = " + savedInstanceState + "\n>");

      super.onCreate (savedInstanceState);

      final boolean authInProgress = savedInstanceState != null &&
	 savedInstanceState.getBoolean (GoogleFit.Auth_Pending);

      googleFit = new GoogleFit (this, authInProgress);

      com.krischik.Log.d (TAG, "- onCreate");
      return;
   } // onCreate

   @Override
   protected void onSaveInstanceState (@NotNull final android.os.Bundle outState)
   {
      com.krischik.Log.d (
	 TAG,
	 "+ onSaveInstanceState" + "\n> outState = " + outState + "\n>");

      super.onSaveInstanceState (outState);

      outState.putBoolean (GoogleFit.Auth_Pending, googleFit.getAuthentication_In_Progress ());

      com.krischik.Log.d (TAG, "- onSaveInstanceState");
      return;
   } // onSaveInstanceState

   @Override
   protected void onStart ()
   {
      com.krischik.Log.d (TAG, "+ onStart)");
      super.onStart ();
      // Connect to the Fitness API

      doConnect (false);
      googleFit.connect ();

      com.krischik.Log.d (TAG, "- onStart");
      return;
   } // onStart

   @Override
   protected void onStop ()
   {
      com.krischik.Log.d (TAG, "+ onStop");
      super.onStop ();

      googleFit.disconnect (/* disable => */false);

      com.krischik.Log.e (TAG, "- onStop");
      return;
   } // onStop

   @android.support.annotation.NonNull
   @Override
   public String toString ()
   {
      return "MainActivity{" +
	 "googleFit=" + googleFit +
	 ", mainFragment=" + mainFragment +
	 '}';
   }
} // MainActivity

// vim: set nowrap tabstop=8 shiftwidth=3 softtabstop=3 expandtab textwidth=96 :
// vim: set fileencoding=utf-8 filetype=java foldmethod=syntax spell spelllang=en_gb :
