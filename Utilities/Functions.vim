""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" {{{1 """""""""""
" Copyright © 2005 … 2012  Martin Krischik
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" This program is free software; you can redistribute it and/or
" modify it under the terms of the GNU General Public License
" as published by the Free Software Foundation; either version 2
" of the License, or (at your option) any later version.
"
" This program is distributed in the hope that it will be useful,
" but WITHOUT ANY WARRANTY; without even the implied warranty of
" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
" GNU General Public License for more details.
"
" You should have received a copy of the GNU General Public License
" along with this program; if not, write to the Free Software
" Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" }}}1 """""""""""

function s:Merge_1 ()
   /\V<<<<<<</		   delete
   /\V=======/,/\V>>>>>>>/ delete
endfunction Merge_1

function s:Merge_2 ()
   /\V<<<<<<</,/\V=======/ delete
   /\V>>>>>>>/		   delete
endfunction Merge_2

function s:Convert_Command_To_Cmd ()
   set filetype=btm
   set fileformat=dos

   $	 substitute /zsh/btm/e
   $	 substitute /unix/dos/e

   1,5	 substitute  /^# /::/g
   1,5	 substitute  /#/:/g

   $-3,$ substitute  /^# /::/g
   $-3,$ substitute  /#/:/g

   5 append
@ECHO OFF

IF NOT "%@Eval[2 + 2]%" == "4" (ECHO ^e[42mYou need TakeCommand [http://www.jpsoft.com] to execute this batch file.^e[m & EXIT /B 1)

IFF NOT DEFINED %[PROJECT_HOME] THEN
    CALL %@Path[%_BatchName]\Setup.cmd
ENDIFF

SETLOCAL
    ON CONDITION ERRORLEVEL NE 0    CANCEL
    ON BREAK                        CANCEL
    ON ERRORMSG                     CANCEL
.

   $-3 append
ENDLOCAL
.

   6,$-4 substitute /\Vtypeset -x -g/SET/
   6,$-4 substitute /\<if\>/IF/
   6,$-4 substitute /\<pushd\>/PUSHD/
   6,$-4 substitute /\<popd\>/POPD/
   6,$-4 substitute /\<done\>/ENDDO/
   6,$-4 substitute /\<fi\>/ENDIFF/
   6,$-4 substitute /; \<then\>/ THEN/
   6,$-4 substitute /${\(.\{-}\)}/%[\1]/g
   6,$-4 substitute /\<if\> \<test\> -x/IFF EXIST/
   6,$-4 substitute /\<alias\>/ALIAS/
endfunction Convert_Command_To_Cmd

function s:Convert_Dependency () range
   execute a:firstline . "," . a:lastline . ' substitute /\(\k*\) [\'"]\(.\{-}\):\(.\{-}\):\(.\{-}\)[\'"]/\1 (\r      [group: "\2", name: "\3", version: "\4"]\r   )/eg' 
endfunction

function s:Java_To_CSharp ()
   %substitute /Log.e(.*.TAG, /Logger.Error(/g
   %substitute /final .* \(\k*\) = /var \1 = /
   %substitute /\([( ]\)final /\1/g
   %substitute /@\<NotNull\>/ /g
   %substitute /@\<Nullable\>/ /g
   %substitute /\<Object\>/in object/g
   %substitute /\<String\>/in string/g
   %substitute /\<CharSequence\>/in string/g
   %substitute /\<boolean\>/bool/g
   %substitute /\<IllegalArgumentException\>/System.InvalidOperationException/g
   %substitute /\<IllegalArgumentException\>/System.ArgumentException/g

   %substitute /trim()/Trim()/g
   %substitute /length()/Length/g

   %substitute /\V\\u00ab/«/g
   %substitute /\V\\u00bb/»/g

   %global /\V@Contract/      delete
   %global /^[ /]*@NotNull$/  delete
   %global /@param/	      .,+1 join
   %global /@throws/	      .,+1 join

   %substitute /@param \(\k*\) \(.*\)/<param name="\1">\2<\/param>/
   %substitute /@throws \(\k*\) \(.*\)/<throws name="\1">\2<\/throws>/
   %substitute /@throws \([A-Za-z.]*\) \(.*\)/<throws name="\1">\2<\/throws>/
endfunction Java_To_CSharp 

function s:Convert_Buildpipeline ()
   %substitute /\V"id": 20/"id": 4/g
   %substitute /\V"id": 23/"id": 3/g
   %substitute /\V"id": 24/"id": 5/g
   %substitute /\V"bf4c942e-30db-4ef3-8a76-ba20a644b029"/"95ceb372-ef89-4553-85f5-83b6499564dc"/
endfunction Convert_Buildpipeline

function Search_Android_Log ()
   vimgrep /\C\v(  System.out.println)|(  net.sourceforge.uiq3.Log.[dv])|(  android.util.Log.[dv])/  **/src/**/*.java
   copen
endfunction Search_Log

command ConvertCommandToCmd	 call <SID>Convert_Command_To_Cmd ()
command JavaToCSharp		 call <SID>Java_To_CSharp ()
command ConvertBuildpipeline	 call <SID>Convert_Buildpipeline ()
command ConvertDependency	 call <SID>Convert_Dependency ()
command Merge1			 call <SID>Merge_1 ()
command Merge2			 call <SID>Merge_2 ()
command SearchAndroidLog	 call Search_Android_Log ()

46amenu <silent> Plugin.Convert.Command\ To\ Cmd      :ConvertCommandToCmd<CR>
46amenu <silent> Plugin.Convert.Java\ To\ CSharp      :JavaToCSharp<CR>
46amenu <silent> Plugin.Convert.Buildpipeline	      :ConvertBuildpipeline<CR>
46amenu <silent> Plugin.Convert.Convert\ Dependency   :ConvertBuildpipeline<CR>
46amenu <silent> Plugin.Merge.Use\ First	      :Merge1<CR>
46amenu <silent> Plugin.Merge.Use\ Second	      :Merge2<CR>
45amenu <silent> Plugin.Convert.AndroidLogger	      :Convert_Android_Logger<CR>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" {{{1 """""""""""
" vim: set nowrap tabstop=8 shiftwidth=3 softtabstop=3 noexpandtab textwidth=96 :
" vim: set fileformat=unix fileencoding=utf-8 filetype=vim foldmethod=marker 
" vim: spell spelllang=en_gb :
