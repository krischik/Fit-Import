
// Jave logr

METHOD_NAME = methodName()
result ?? variableOfType(methodReturnType())

// Kotlin logm

Method              groovyScript("'\"+ ' + _1 + '\"'", kotlinFunctionName())
Parameter_Names     groovyScript("def params = _1.collect {'> ' + it + ' = %1\\\\$s'}.join('\" + \\n\"'); return (params.empty  ? '' : '+ \\n\"' + params + '\"')", functionParameters())
Parameter           groovyScript("def params = _1.collect {it}.join(',\\n'); return (params.empty  ? '' : ',\\n' + params)", functionParameters())

// Kotlin logr

METHOD_NAME = kotlinFunctionName()
result = groovyScript("def retval = _1); return retval.empty ? '' : '\\\\n> retval = %1$s\", ' + retval")

def retval = variableOfType(methodReturnType()); return retval.empty ? '' : '\\\\n> retval = %1$s\", ' + retval

